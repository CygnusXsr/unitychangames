﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityChanController : MonoBehaviour {
//Unityちゃんのアニメーションを格納
Animator animator;
void Start()
{
//UnityちゃんのAnimatorコンポーネントを取得
this.animator = GetComponent<Animator>();
}
void Update()
{
       
}
//オブジェクトに触れたときの処理
private void OnTriggerEnter(Collider other)
{
//オブジェクトがGoalの場合
if(other.gameObject.name == "Goal")
{
//勝利モーションを表示
animator.SetBool("Win", true);
}
}
}
